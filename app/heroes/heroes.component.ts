import { Component,OnInit } from "@angular/core";

import {HeroDetailComponent} from "../hero-detail/hero-detail.component";
import {Hero} from "../models/hero";
import {HeroService} from "../hero.service";

@Component({
    selector: 'my-heroes',
    templateUrl:'app/heroes/heroes.component.html',
        directives:[HeroDetailComponent],
        styleUrls:['app/heroes/heroes.component.css']
})
export class HeroesComponent implements OnInit{
    title="Heroes for days";
    selectedHero: Hero;
    heroes: Hero[];
    constructor(private heroService: HeroService){}
    
    ngOnInit(){
        this.getHeroes()
    }
    
    getHeroes(){
        this.heroService.getHeroes().then(heroes => this.heroes = heroes);
    }
    
    onSelect(hero:Hero){this.selectedHero = hero;}
}